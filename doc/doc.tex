\documentclass[12pt]{article}
\usepackage{sbc-template}
\usepackage{inconsolata}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage{url}
\usepackage{enumitem}
\usepackage[brazil]{babel}   
\usepackage[utf8]{inputenc}
\newcommand{\figref}[1]{\textbf{figura \ref{#1}}}
\newcommand{\tabref}[1]{\textbf{tabela \ref{#1}}}

\begin{document} 

\title{Aprendizado de Máquina\\Trabalho Prático 2: Bagging e Boosting}
\address{Departamento de Ciência da Computação\\Universidade Federal de Minas Gerais (UFMG)}

\author{Arthur Câmara\inst{1}, Gustavo Brunoro\inst{1}, Luís Lizardo\inst{1}\email{\{acamara,brunoro,lizardo\}@dcc.ufmg.br}}
\maketitle

\section{Introdução}
\label{intro}
% TP1 e base titanic
No primeiro trabalho prático da disciplina modelamos o problema da classificação da base de dados
contendo dados dos passageiros do Titanic entre sobreviventes e não-sobreviventes. Neste segundo
trabalho prático foi proposta uma análise mais aprofundada do problema, com ênfase em duas técnicas
de meta-aprendizado: bagging e boosting.

% planejamento e metodologia (weka)
O desenvolvimento do trabalho foi realizado usando a ferramenta Weka\cite{weka}, desenvolvida na
Universidade de Waikato. O projeto oferece um arcabouço contendo implementações de vários algoritmos
de aprendizado de máquina e utilitários para visualização e análise de dados. 

Na seção \ref{base} é apresentada uma análise estatística do comportamento da base de dados Titanic,
para que em seguida, como descrito na seção \ref{attr_classif}, seja realizada a seleção dos atributos
de classificação e dos algoritmos empregados na solução do problema. A seção \ref{resultados} descreve
e analisa os resultados obtidos na resolução do problema.

\section{Análise dos dados}
\label{base}
A base de dados Titanic contém 891 exemplos rotulados e 418 não-rotulados representando dados de
passageiros do navio. Os atributos fornecidos nestes arquivos são os descritos abaixo; \texttt{name} 
e \texttt{cabin} foram ignorados na solução do problema por não apresentarem informação relevante ao problema.

\begin{description}[style=multiline, font=\texttt, leftmargin=3cm]
  \item[survival] Sobrevivente ou não-sobrevivente.
  \item[pclass]   Classe (1ª, 2ª ou 3ª).
  \item[name]     Nome.
  \item[sex]      Sexo.
  \item[age]      Idade.
  \item[sibsp]    Número de irmãos e/ou cônjuges à bordo.
  \item[parch]    Número de pais e/ou filhos à bordo.
  \item[ticket]   Número da passagem.
  \item[fare]     Tarifa paga.
  \item[cabin]    Número da cabine.
  \item[embarked] Porto de embarque (Cherbourg, Queenstown, Southampton).
\end{description}

% histogramas
A primeira análise realizada foi a da distribuição das classes nos dados de treinamento em relação ao
atributo \texttt{survived}. A figura \ref{histograms} mostra o resultado desta análise.

É possível notar que a distribuição das classes nos exemplos apresenta não é balanceada: há mais exemplos
positivos que negativos. O atributo \texttt{sex} se mostra bastante descriminativo, enquanto
outros, como \texttt{embarked}, se mostram razoavelmente equilibrados.

\begin{figure}[h!]
  \begin{center}
  \includegraphics[width=1.0\linewidth]{img/histogramas.png}
  \caption{Distribuição dos dados da base em relação às classes a serem separadas}
  \label{histograms}
  \end{center}
\end{figure}

% atributos 
A segunda análise realizada é a seleção dos atributos mais relevantes segundo duas métricas: ganho de informação
e chi-square. A primeira mede o impacto de um atributo na entropia da base de dados, enquanto a segunda mede a 
diferença entre os valores presentes e esperado, caso a distribuição dos atributos seja aleatória, em uma base
de dados. Mais informações sobre as métricas podem ser encontradas em \cite{intro-ir}.

A \tabref{attr_selection} mostra o ranking gerado por ambas as métricas de seleção de atributos. 
Não surpreendentemente ambas as métricas resultam na mesma ordenação de atributos. Conforme notado na análise
por histogramas, o atributo mais discriminativo é \texttt{sex}, e \texttt{embarked} está entre os atributos menos
discriminativos. Entretanto, o atributo \texttt{age} ser o menos discriminativo é pouco intuitivo em relação
à descrição do problema.

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{lll}
      Atributo  & Chi-square & InfoGain \\ \hline
      sex       & 263,0506   & 0,2177   \\
      fare      & 115,0758   & 0,0962   \\
      pclass    & 102,889    & 0,838    \\
      sibsp     & 31,0134    & 0,0265   \\
      embarked  & 29,3935    & 0,0209   \\
      parch     & 16,3605    & 0,0154   \\
      age       & 14,3924    & 0,0117   \\
    \end{tabular}
    \caption{Ranking resultante da seleção de atributos}
  \end{center}
  \label{attr_selection}
\end{table}

\section{Classificadores e algoritmos}
\label{attr_classif}

% bagging
\subsection{Bagging}
Bagging, contração de \emph{bootstrap aggregating}, é um método de meta-aprendizado que é baseado na divisão da
base de dados em amostras de mesmo tamanho, que em seguida são usadas para treinar classificadores. 
O resultado da classificação é simplemente o resultado da votação dos classificadores treinados sobre as amostras.
O algoritmo de boosting utilizado neste trabalho e implementado no Weka é descrito em \cite{bagging}, o artigo
original que descreve a técnica.

% boosting
\subsection{Boosting}
Boosting é um método que propões combinação de vários classicadores simples (ou \emph{weak learners}) e agrega-os
de forma a gerar previsões de alta qualidade. O algoritmo escolhido para uso neste trabalho prático é o AdaBoost 
(de \emph{adaptive boosting}), principal algoritmo de boosting disponível na literatura, descrito em \cite{adaboost}.

A técnica consiste em, à cada iteração, treinar um classificador e atualizar os pesos dos exemplos na base de dados
usando o resultado da classificação sobre o recém-treinado modelo; todos os exemplos tem o mesmo peso no início da
execução. A classificação de um exemplo consiste na votação dos classificadores treinados com os votos ponderados 
pelo erro no seu aprendizado.

\section{Resultados}
\label{resultados}
% classificadores
A escolha dos classificadores empregados busca abrangir as diversas categorias de métodos disponíveis na literatura,
sendo usados: Naive Bayes, SVM, REPTree, J48 (C4.5) e JRip. 
Para o uso de boosting também foram incorporados o classificador Decision Stump, que consiste em uma árvore de decisão 
com somente uma regra.

% experimentos
Os experimentos foram realizados com a base rotulada de treino usando validação cruzada com 5 folds. 
Os resultados estão dispostos na tabela \tabref{result}, apresentando a porcentagem média de instâncias classificadas
corretamente e o desvio padrão entre os resultados nos folds.

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{ll}
      Classificador & Acerto médio\\
      \hline
      Bagging JRip & 82.01 $\pm$ 2.62\\
      Bagging REPTree & 81.59 $\pm$ 2.74\\
      AdaBoost JRip& 81.35 $\pm$ 3.12\\
      JRip & 81.10 $\pm$ 2.85\\
      Bagging J48 & 80.81 $\pm$ 2.95\\
      REPTree & 80.56 $\pm$ 3.07\\
      J48 & 79.97 $\pm$ 3.39\\
      AdaBoost REPTree & 79.61 $\pm$ 2.83\\
      AdaBoost DecisionStump & 78.99 $\pm$ 3.82\\
      AdaBoost NaiveBayes & 78.74 $\pm$ 3.31\\
      AdaBoost J48 & 78.17 $\pm$ 3.27\\
      NaiveBayes & 78.05 $\pm$ 3.25\\
      Bagging NaiveBayes & 77.96 $\pm$ 3.37\\
      Bagging DecisionStump & 77.95 $\pm$ 3.37\\
      LibSVM & 68.57 $\pm$ 2.79\\
      Bagging LibSVM & 68.40 $\pm$ 3.06\\
      AdaBoost LibSVM & 67.53 $\pm$ 3.53\\
    \end{tabular}
    \caption{Resultados dos experimentos}
    \label{result}
  \end{center}
\end{table}

% meta
Os resultados mostram que as técnicas de meta-classificação utilizadas de fato conseguiram melhorar o acerto dos
modelos treinados.
Nota-se que o classificador SVM, que é o estado da arte de algoritmos de classificação em vários domínios, apresentou
os piores resultados entre os métodos testados.
Estes resultados se explicam pela sensibildade do SVM aos parâmetros de sua execução, que não foram escopo deste trabalho.

Os próximos experimentos levaram em conta os classificadores REPTree e JRip, por apresentarem os melhores resultados, e o
classificador DecisionStump, por ser o modelo mais simples dentre os avaliados, em relação aos parâmetros dos métodos
de meta-aprendizado.

% adaboost
\begin{figure}[h!]
  \begin{center}
  \includegraphics[width=0.8\linewidth]{img/ada.png}
  \caption{Impacto do número de iterações do AdaBoost}
  \label{adaboost}
  \end{center}
\end{figure}

Como esperado, o algoritmo AdaBoost resulta em ganho de qualidade de classificação em modelos simples.
A \figref{adaboost} mostra a variação da porcentagem média de instâncias corretamente classificadas para
os algoritmos JRip, REPTree e DecisionStump.
Somente o algoritmo DecisionStump apresenta ganhos com o aumento do número de iterações, e estes ganhos
tem um limite superior em torno de 80.2\%.

% bagging
\begin{figure}[h!]
  \begin{center}
  \includegraphics[width=0.8\linewidth]{img/bagging.png}
  \caption{Impacto do número de iterações de Bagging}
  \label{bagging}
  \end{center}
\end{figure}

A \figref{bagging} mostra que o Bagging não apresenta grande influência sobre os reultados de classificadores
simples como o DecisionStump.
Apesar de variações ocorrerem com algoritmos mais complexos após o uso de Bagging, ainda sim não foi possível
identificar um padrão nos ganhos obtidos.
Acima de 30 iterações de ambos os algoritmos JRip e REPTree os ganhos de precisão se estabilizam, variando
em torno do valor de sua convergência, em torno de 82.25\% e 81.70\% respectivamente.

\section{Conclusão}
\label{conclusao}
Neste trabalho foi apresentada uma análise de duas técnicas de meta-aprendizado: Boosting, pelo algoritmo
AdaBoost, e Bagging.
Foram obtidos resultados que revelam a eficácia destas técnicas e quais classificadores melhor se adequam
à sua aplicação.
Também foram apresentados dados sobre o comportamento destes algoritmos em relação ao número de iterações
utilizadas, e a estabilidade dos resultados atingidos por destes métodos.

\bibliography{doc}
\bibliographystyle{alpha}

\end{document}
