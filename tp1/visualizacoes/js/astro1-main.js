var nodes = {};

var count = {};

for(i in links) {
  var link = links[i];
  count[link.source] = (typeof count[link.source] === "undefined") ? 1 : count[link.source] + 1;
  count[link.target] = (typeof count[link.target] === "undefined") ? 1 : count[link.target] + 1;
}

// Compute the distinct nodes from the links.
links.forEach(function(link) {
  link.source = nodes[link.source] || (nodes[link.source] = {name: link.source});
  link.target = nodes[link.target] || (nodes[link.target] = {name: link.target});
});

var max = 0;

for (i in nodes) {
  var number = count[nodes[i].name];
  if(number > max) max = number;
  nodes[i]['count'] = number;
}

// Width and height
var w = 1400,
    h = 800;

//append SVG element
var svg = d3.select("body").append("svg:svg")
    .attr("width", w)
    .attr("height", h)

var edges = links.length;
var distance_value = 1.3 * 300/Math.sqrt(edges);
var charge_value = 1.3 * (-1) * 200/Math.sqrt(edges);

console.log("Edges: "+edges);
// console.log("Distance: "+distance_value);
// console.log("Charge: "+charge_value);
console.log("Max: "+max);

// force nodes distance
var force = d3.layout.force()
    .nodes(d3.values(nodes))
    .links(links)
    .size([w, h])
    .linkDistance(distance_value)   //distance
    .charge(charge_value)           //away from center
    .on("tick", tick)
    .start();

// edges
var path = svg.append("svg:g").selectAll("path")
    .data(force.links())
  .enter().append("svg:path")
    .attr("class", function(d) { return "link"; });

// circles
var circle = svg.append("svg:g").selectAll("circle")
    .data(force.nodes())
  .enter().append("svg:circle")
    .attr("r", function(d) { return (1.5 + (0.5 * d.count)); })
    .attr("class", function(d) { return (d.count >= max) ? "max" : ""; })
    .call(force.drag);

//drawing function
function tick() {
  path.attr("d", function(d) {
    var dx = d.target.x - d.source.x,
        dy = d.target.y - d.source.y,
        dr = Math.sqrt(dx * dx + dy * dy);
    return "M" + d.source.x + "," + d.source.y + "," + d.target.x + "," + d.target.y;
  });

  circle.attr("transform", function(d) {
    return "translate(" + d.x + "," + d.y + ")";
  });
}