#!/usr/bin/env python
from argparse import ArgumentParser
from numpy import mean, std, arange
from math import ceil, floor, sqrt

import json_config

import matplotlib
import pylab
import sys
import re

def handle_missing_attributes(examples):
    return [e for e in examples if None not in e]

def collect_indexes(array, indexes):
    return [array[i] for i in indexes]

def parse_input_file(input_file, prepare_example):
    input_file.readline() # skip header
    all_examples = [prepare_example(e) for e in input_file]
    full_examples = handle_missing_attributes(all_examples)

    return full_examples

def plot_data(labels, data):
    matplotlib.rcParams.update({'font.size': 10})

    data_by_attr = zip(*data)
    fig_count = 0

    plot_x = int(floor(sqrt(len(labels))))
    plot_y = int(ceil(len(labels) / float(plot_x)))
    f, axarr = pylab.subplots(plot_x, plot_y)

    for label, attr in zip(labels, data_by_attr):
        x = fig_count / plot_y
        y = fig_count % plot_y

        attr = [a for a in attr if a != None]

        size = max(attr) - min(attr)

        axarr[x, y].hist(attr, size / 10 if size > 10 else size + 1)
        axarr[x, y].set_title(label)
        
        fig_count += 1

    pylab.show()

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input_file", dest="input_file", required=True,
                        help="CSV file containing training examples", metavar="FILE")
    parser.add_argument("-c", "--config", dest="config", required=True,
                        help="Description of the CSV file containing training examples", metavar="FILE")
    return parser.parse_args()

args = parse_arguments()

input_file = open(args.input_file, "r")
config = json_config.load_file(open(args.config, "r"))

data = parse_input_file(input_file, config['prepare_example'])

plot_data(config['attr_header'], data)
