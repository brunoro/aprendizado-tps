from argparse import ArgumentParser
import random

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", dest="input", required=True,
                        help="Input File", metavar="FILE")
    parser.add_argument("-o", "--output", dest="output", required=True,
                        help="Output File", metavar="FILE")
    parser.add_argument("-p", "--probability", dest="probability", type=float, default=0.1,
                        help="Probability of including a certain relationship into the output file ", metavar="FLOAT")

    return parser.parse_args()

args = parse_arguments()

input = open(args.input, "r")
output = open(args.output, "w")
probability = args.probability

random.seed()

lines = input.readlines()
print len(lines)

output.write("var links = [\n");
c = 0
first = True
for i in lines:
	r = random.random()
	if(r < probability):
		if (first):
			first = False
		else:
			output.write(',\n');
		nodes = i.strip().split('	')
		c = c+1
		output.write('{source: "'+nodes[0]+'", target: "'+nodes[1]+'", type: '+nodes[2]+'}')


output.write("\n];")
print c