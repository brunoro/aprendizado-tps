var w = 1280,
    h = 800,
    rx = w / 2,
    ry = h / 2,
    m0,
    rotate = 0;

var splines = [];

var cluster = d3.layout.cluster()
    .size([360, ry - 120])
    .sort(function(a, b) { return d3.ascending(a.key, b.key); });

var bundle = d3.layout.bundle();

var line = d3.svg.line.radial()
    .interpolate("bundle")
    .tension(.85)
    .radius(function(d) { return d.y; })
    .angle(function(d) { return d.x / 180 * Math.PI; });

var div = d3.select("body").insert("div", "h2")
    .style("top", "-80px")
    .style("left", "-160px")
    .style("width", w + "px")
    .style("height", w + "px")
    .style("position", "absolute");

var svg = div.append("svg:svg")
    .attr("width", w)
    .attr("height", w)
  .append("svg:g")
    .attr("transform", "translate(" + rx + "," + ry + ")");

svg.append("svg:path")
    .attr("class", "arc")
    .attr("d", d3.svg.arc().outerRadius(ry - 120).innerRadius(0).startAngle(0).endAngle(2 * Math.PI));

var loadThis = function(classes) {

  var nodes = cluster.nodes(packages.root(classes)),
      links = packages.connections(nodes),
      splines = bundle(links);

  // console.log(links);

  var max = 0;
  count = 0;
  count2 = 0;
  for (i in nodes) {
    thisnum = nodes[i].size;
    if(thisnum > max) max = thisnum;
  }
  for (i in nodes) {
    thisnum = nodes[i].size;
    if(thisnum > 0) count++;
    if(thisnum == max) count2++;
  }

  console.log(count);
  console.log(count2);

  var path = svg.selectAll("path.link")
      .data(links)
    .enter().append("svg:path")
      .attr("class", function(d) {
        var classes = "";
        if (d.source.size == max || d.target.size == max) {
          classes = "max link source-" + d.source.key + " target-" + d.target.key;
        } else {
          classes = "link source-" + d.source.key + " target-" + d.target.key;
        } 
        if(d.type == 1) {
          classes += " friend ";
        } else {
          classes += " enemy ";
        }

        return classes;
      })
      .attr("d", function(d, i) { return line(splines[i]); });

  svg.selectAll("g.node")
      .data(nodes.filter(function(n) { return !n.children; }))
    .enter().append("svg:g")
      .attr("class", "node")
      .attr("id", function(d) { return "node-" + d.key; })
      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; });

  d3.select("input[type=range]").on("change", function() {
    line.tension(this.value / 100);
    path.attr("d", function(d, i) { return line(splines[i]); });
  });
};


loadThis(edges);

function updateNodes(name, value) {
  return function(d) {
    if (value) this.parentNode.appendChild(this);
    svg.select("#node-" + d[name].key).classed(name, value);
  };
}

function cross(a, b) {
  return a[0] * b[1] - a[1] * b[0];
}

function dot(a, b) {
  return a[0] * b[0] + a[1] * b[1];
}

    