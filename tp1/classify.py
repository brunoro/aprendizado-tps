#!/usr/bin/env python
from argparse import ArgumentParser
from sklearn import cross_validation
from numpy import mean, std, arange
from math import trunc

import json_config

import matplotlib
import pylab
import sys
import re

def handle_missing_attributes(examples):
    return [e for e in examples if None not in e]

def collect_indexes(array, indexes):
    return [array[i] for i in indexes]

def parse_input_file(input_file, prepare_example):
    input_file.readline() # skip header
    all_examples = [prepare_example(e) for e in input_file]
    full_examples = handle_missing_attributes(all_examples)

    features = [e[1:] for e in full_examples]
    samples = [e[0] for e in full_examples]

    return (features, samples)

def test_classifier(classifier, test_data, n_folds):
    features, samples = test_data
    folds = cross_validation.StratifiedKFold(samples, n_folds)
    acc = []
    for indexes in folds:
        features_train, features_test = [collect_indexes(features, i) for i in indexes]
        samples_train, samples_test = [collect_indexes(samples, i) for i in indexes]
        clf = classifier()
        clf.fit(features_train, samples_train)
        
        acc.append(clf.score(features_test, samples_test))

    return acc

def show_results(classifiers, results):
    ind = arange(len(classifiers))
    
    matplotlib.rcParams.update({'font.size': 10})
    pylab.figure(1)

    means = [mean(r) for r in results]
    stds  = [std(r) for r in results]

    first = lambda k: k[0]
    s_zipped = sorted(zip(means, stds, classifiers), key=first)
    s_means, s_stds, s_classifiers = zip(*s_zipped)
    rects = pylab.barh(ind, s_means, align='center', xerr=s_stds, ecolor='r')

    pylab.grid(True)
    pylab.ylabel('Classifier')
    pylab.xlabel('Mean accuracy')
    pylab.xlim([0, 1])
    pylab.title('Mean accuracy by classifier')
    pylab.yticks(ind, [c.__name__ for c in s_classifiers])

    pylab.subplots_adjust(left=0.25)

    pylab.show()


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input_file", dest="input_file", required=True,
                        help="CSV file containing training examples", metavar="FILE")
    parser.add_argument("-c", "--config", dest="config", required=True,
                        help="Description of the CSV file containing training examples", metavar="FILE")
    parser.add_argument("-f", "--folds", dest="n_folds", type=int, default=5,
                        help="Number of folds used for cross-validation", metavar="INT")

    return parser.parse_args()

args = parse_arguments()

input_file = open(args.input_file, "r")
config_file = open(args.config, "r")

config = json_config.load_file(config_file)

data = parse_input_file(input_file, config['prepare_example'])
results = [test_classifier(c, data, args.n_folds) for c in config['classifiers']]
show_results(config['classifiers'], results)
