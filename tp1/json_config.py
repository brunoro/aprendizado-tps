import importlib
import json
import re

def int_or_none(a): 
    if len(a) > 0:
        return int(float(a))
    else:
        return None

def list_or_none(l): 
    d = dict(zip(l, range(len(l))))
    return (lambda k: d[k] if k in d else None)

# returns a function to parse an attribute from an example
def function_from_model(attr_type):
    if attr_type == 'int':
        return int_or_none

    elif type(attr_type) is list:
        return list_or_none([at.strip() for at in attr_type])

    # ignore otherwise
    return None 

# returns a function to parse an example based on a list of model strings
def load_model(model):
    attr_header = [k for k, v in model if v]
    funs = [function_from_model(v) for _, v in model]

    def prepare_example(raw):
        clean_name = re.sub('"(.*), (.*)"', '"\\2 \\1"', raw)
        data = [s.strip() for s in clean_name.split(',')]

        return [fun(d) for fun, d in zip(funs, data) if fun]

    return attr_header, prepare_example

# returns the class object by the import path specified by s
# and imports the required module
def class_from_string(s):
    sp = s.split(".")
    module_name, class_name = '.'.join(sp[:-1]), sp[-1]
    module = importlib.import_module(module_name)

    return getattr(module, class_name)

def load_file(config_file):
    raw_config = json.load(config_file)
    config = {}

    config['classifiers'] = [class_from_string(c) for c in raw_config['classifiers']]
    config['attr_header'], config['prepare_example'] = load_model(raw_config['model'])

    return config
