from argparse import ArgumentParser
import random

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", dest="input", required=True,
                        help="Input File", metavar="FILE")
    parser.add_argument("-o", "--output", dest="output", required=True,
                        help="Output File", metavar="FILE")
    parser.add_argument("-p", "--probability", dest="probability", type=float, default=0.1,
                        help="Probability of including a certain relationship into the output file ", metavar="FLOAT")

    return parser.parse_args()

args = parse_arguments()

input = open(args.input, "r")
output = open(args.output, "w")
probability = args.probability

random.seed()

lines = input.readlines()
print len(lines)

lines2 = {}

c = 0
for i in lines:
	r = random.random()
	if(r < probability):
		c = c+1
		nodes = i.strip().split('	')
		lines2.setdefault(nodes[0], {})
		lines2.setdefault(nodes[1], {})
		lines2[nodes[0]][nodes[1]] = 1;
		lines2[nodes[1]][nodes[0]] = 1;

#calculate max
max = 1
for (k, l) in lines2.items():
	size = len(l)
	if size > max: max = size


output.write("var json_classes = [\n");
first = True
for (k, l) in lines2.items():

	sizeDict = len(l)
	if(sizeDict == 1 or sizeDict < max/2):
		sizeDict = 0

	if(first):
		first = False
	else:
		output.write(",\n");

	imports = '';
	if sizeDict > 0: 
		firstTwo = True
		for (j, i) in l.items():
			if(firstTwo):
				firstTwo = False
			else:
				imports += ','
			imports += '"'+j+'"'

	output.write('{"name": "'+k+'", "size": '+str(sizeDict)+', "imports": ['+imports+'] }');

output.write("\n]");

print c
print "Max: " + str(max)