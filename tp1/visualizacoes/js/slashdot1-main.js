var nodes = {};

var count = {};
var count_friend = {};
var count_enemy = {};
var count_friend_edges = 0;
var count_enemy_edges = 0;

for(i in links) {
  var link = links[i];
  count[link.source] = (typeof count[link.source] === "undefined") ? 1 : count[link.source] + 1;
  count[link.target] = (typeof count[link.target] === "undefined") ? 1 : count[link.target] + 1;

  count_friend[link.source] = (typeof count_friend[link.source] === "undefined") ? 0 : count_friend[link.source];
  count_friend[link.target] = (typeof count_friend[link.target] === "undefined") ? 0 : count_friend[link.target];
  count_enemy[link.source] = (typeof count_enemy[link.source] === "undefined") ? 0 : count_enemy[link.source];
  count_enemy[link.target] = (typeof count_enemy[link.target] === "undefined") ? 0 : count_enemy[link.target];

  if(link.type == 1) {

    count_friend[link.source] = count_friend[link.source] + 1;
    count_friend[link.target] = count_friend[link.target] + 1;

    count_friend_edges += 1;

  } else {

    count_enemy[link.source] = count_enemy[link.source] + 1;
    count_enemy[link.target] = count_enemy[link.target] + 1;

    count_enemy_edges += 1;

  }

}

var group_count = 0;
var groups_map = [];
var groups_max = 0;

var groups = 1;

// Compute the distinct nodes from the links.
links.forEach(function(link) {

   if(typeof link.source['group'] !== "undefined") link.source['group']

  node1 = 

  link.source = nodes[link.source] || (nodes[link.source] = {name: link.source, type: link.type} );
  link.target = nodes[link.target] || (nodes[link.target] = {name: link.target, type: link.type} );

});

for(i in groups_map) {
  if(groups_map[i] > groups_max) groups_max = groups_map[i];
}

var max = 0;
var max_friends = 0;
var max_enemies = 0;

//find max
for (i in nodes) {
  var number = count[nodes[i].name];
  var number_friends = count_friend[nodes[i].name];
  var number_enemies = count_enemy[nodes[i].name];
  if(number > max) max = number;
  if(number_friends > max_friends) max_friends = number_friends;
  if(number_enemies > max_enemies) max_enemies = number_enemies;
  nodes[i]['count'] = number;
  nodes[i]['count_friends'] = number_friends;
  nodes[i]['count_enemies'] = number_enemies;
}

// Width and height
var w = 1400,
    h = 800;

//append SVG element
var svg = d3.select("body").append("svg:svg")
    .attr("width", w)
    .attr("height", h)

var edges = links.length;
var distance_value = 1.4 * 300/Math.sqrt(edges);
var charge_value = 1.4 * (-1) * 200/Math.sqrt(edges);

console.log("Edges: "+edges);
console.log("Friends: "+count_friend_edges);
console.log("Enemies: "+count_enemy_edges);
console.log("Max Group: "+groups_max);
console.log("Max: "+max);
console.log("Max Friends: "+max_friends);
console.log("Max Enemies: "+max_enemies);

// force nodes distance
var force = d3.layout.force()
    .nodes(d3.values(nodes))
    .links(links)
    .size([w, h])
    .linkDistance(distance_value)   //distance
    .charge(charge_value)           //away from center
    .on("tick", tick)
    .start();

// edges
var path = svg.append("svg:g").selectAll("path")
    .data(force.links())
    .enter().append("svg:path")
    .attr("class", function(d) {
      var classes = "";
      classes += (d.type == 1) ? "link friend" : "link enemy" ;

      return classes;
    });

// circles
var circle = svg.append("svg:g").selectAll("circle")
    .data(force.nodes())
  .enter().append("svg:circle")
    .attr("r", function(d) { return (1.5 + (0.5 * d.count)); })
    .attr("class", function(d) {
      var classes = ""
      classes += (d.count >= max) ? " max " : "";
      classes += (d.count_friends >= max_friends) ? " maxfriends " : "";
      classes += (d.count_enemies >= max_enemies) ? " maxenemies " : "";
      return classes;
    })
    .call(force.drag);

//drawing function
function tick() {
  path.attr("d", function(d) {
    var dx = d.target.x - d.source.x,
        dy = d.target.y - d.source.y,
        dr = Math.sqrt(dx * dx + dy * dy);
    return "M" + d.source.x + "," + d.source.y + "," + d.target.x + "," + d.target.y;
  });

  circle.attr("transform", function(d) {
    return "translate(" + d.x + "," + d.y + ")";
  });
}