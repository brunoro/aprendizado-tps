# TP1 de Aprendizado de Máquina - 2013/1

## Dependências
Python com os módulos scikit\_learn, numpy, scipy e matplotlib instalados.
O jeito mais fácil de instalá-los é usando o [pip](https://pypi.python.org/pypi/pip)

## Classificação
Estão disponíveis no diretório raiz dois scripts: classify.py e histograms.py

O script classify.py treina classificadores diferentes para os dados de entrada
e exibe o acurácia do treinamento com validação cruzada em um gráfico de barras.
O script histograms.py exibe a distribuição dos atributos fornecidos como histogramas.

Ambos recebem os mesmos parâmetros básicos -i e -c como no exemplo de chamada:
  
    $ python classify.py -i titanic/train.csv -c titanic/config.json

O arquivo de entrada especificado com -i é um csv onde a primeira coluna é o atributo a 
ser previsto e a primeira linha é um cabeçalho que é ignorado.

O arquivo de configuração especificado pela opção -c é umé um json com dois atributos: 
"model", que lista os atributos presentes no arquivo de entrada e seu tipo, que podem ser:

* `null` para atributos ignorados
* `int` para atributos inteiros
* uma lista de strings para atributos categóricos, como `["male", "female"]`
