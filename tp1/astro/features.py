#!/usr/bin/env python
from argparse import ArgumentParser
import random

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", dest="input_file", required=True,
                        help="Input File", metavar="FILE")
    parser.add_argument("-o", "--output", dest="output_file", required=True,
                        help="Output File", metavar="FILE")
    parser.add_argument("-p", "--probability", dest="probability", type=float, default=0.1,
                        help="Probability of including an edge", metavar="FLOAT")
    parser.add_argument("-f", "--false-edges", dest="falseedges", type=float, default=0.00001,
                        help="Probability of including false edges", metavar="FLOAT")
    return parser.parse_args()

# Get list of adjacency for each node
def get_adjacency_list(lines, prob):
	edges = {}

	for i in lines:
		r = random.random()
		if(r < prob):
			n = i.strip().split('	')

			edges.setdefault(n[0], set())
			edges.setdefault(n[1], set())
            
			edges[n[0]].add(n[1])
			edges[n[1]].add(n[0])

	return edges

# Get Features
# edges is a list of adjacency for each node
# desired_features is a list of functions
def get_features(edges, false_edges, desired_features):
	all_features = []
	nodes = edges.keys()
	num_edges = len(nodes)

	for i in range(0, num_edges):
		for j in range(i+1, num_edges):
			# compared nodes
			node1 = nodes[i]
			node2 = nodes[j]
			adjacency = 1 if node2 in edges[node1] else 0
			
            # do we want to include a false edge?
			if adjacency == 0:
				r = random.random()
				if(r > false_edges): # skip this edge
					continue

			# if we are here, we want to include this edge:
			# building classification features for these 2
			# features = [class, similarity, ...]
			features = [adjacency]

			# all features
			features += [f(edges, node1, node2) for f in desired_features]

			# append to global array	
			all_features.append(features)

	return all_features

def print_matrix(matrix, output):
	for line in matrix:
		new_line = [str(x) for x in line]
		join_char = ','
		new_line = join_char.join(new_line) + '\n'
		output.write(new_line)

# === Features ===

# Similarity = Number of common edges
def similarity(edges, node1, node2):
	return len(edges[node1].intersection(edges[node2]));

# Difference = Number of different edges
def difference(edges, node1, node2):
	return len(edges[node1].symmetric_difference(edges[node2]));

# Jaccard Index
def jaccard_index(edges, node1, node2):
	similar = similarity(edges, node1, node2)
	precision = 10000
	# jaccard = intersection / union
	jaccard = similar / float(len(edges[node1]) + len(edges[node2]) - similar)
	return int(jaccard * precision)
    
# Returns true if two nodes are in a connect subgraph
def same_group(edges, node1, node2, group_set = []):
	if not group_set:
		group_set.append(map_groups(edges))
	group = group_set[0]

	return int(group[node1] == group[node2])

# Create a map of connected subgraph
def map_groups(graph):
    group = 0
    group_map = {}

    for i in graph:
        if group_map.has_key(i):
            tmpgroup = group_map[i]
        else:
            tmpgroup = group
            group_map[i] = tmpgroup
            group += 1
            
        for j in graph[i]:
            group_map[j] = tmpgroup    
    return group_map
            
# === Main ===

# Parse Arguments
args = parse_arguments()
input_file = open(args.input_file, "r")
output_file = open(args.output_file, "w")
probability = args.probability
false_edges = args.falseedges
 
# seed
random.seed()
 
edges = get_adjacency_list(input_file, probability)
 
# Compute Features
features_set = [similarity, difference, jaccard_index, same_group]
features = get_features(edges, false_edges, features_set)

# Write output file
print "Number of nodes: " + str(len(edges))
print "Writing output file..."
  
print_matrix(features, output_file)
  
print "DONE!"
